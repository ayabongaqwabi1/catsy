const config = {
    siteName: "Catsy",
    routes: [
        {
            name: "Breeds",
            href: "/breeds"
        },
        {
            name: "Categories",
            href: "/categories"
        },
        {
            name: "Favorites",
            href: "/favorites"
        }
    ]
};

export default config;