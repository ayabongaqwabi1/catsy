import request from "axios";
import { isEmpty, isNil, head } from "ramda";

const exists = (i) => !isNil(i) && !isEmpty(i);

const config = {
  headers: {
    "x-api-key": process.env.CAT_API_KEY,
  },
};

export const getBreeDatadFromQuery = async (breed) => {
  const BREEDS_URL = "https://api.thecatapi.com/v1/breeds";
  const BreedsRequest = await request.get(BREEDS_URL, config);
  const breeds = BreedsRequest.data;
  const getBreedData = (item) =>
    item
      ? { breedId: item.id, breedName: item.name }
      : { breedId: null, breedName: null };
  const breedData = exists(breed)
    ? getBreedData(
        head(
          breeds.filter(
            (b) =>
              b.name.toLowerCase() === breed.toLowerCase() ||
              b.id.toLowerCase() === breed.toLowerCase()
          )
        )
      )
    : { breedId: null, breedName: null };
  return breedData;
};

export const getCategoryDataFromQuery = async (category) => {
  const CATEGORIES_URL = "https://api.thecatapi.com/v1/categories";
  const CategoriesRequest = await request.get(CATEGORIES_URL, config);
  const categories = CategoriesRequest.data;
  const getCategoryData = (item) =>
    item
      ? {
          categoryId: item.id,
          categoryName: item.name,
        }
      : { categoryId: null, categoryName: null };
  const categoryData = exists(category)
    ? getCategoryData(
        head(
          categories.filter(
            (c) => c.name.toLowerCase() === category.toLowerCase()
          )
        )
      )
    : { categoryId: null, categoryName: null };
  return categoryData;
};
