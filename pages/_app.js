import "tailwindcss/tailwind.css";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <NextNProgress color="hotpink"  />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
