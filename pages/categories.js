
import Image from 'next/image'
import styled from 'styled-components';
import Layout from '../components/layout';
import request from 'axios';
import Card from '../components/SimpleCard';
import { isEmpty, isNil, view } from 'ramda';
import { useRouter } from 'next/router'
import Header from "../components/PageHeader";


const CategoriesPage = ({ categories }) => {
  const router = useRouter()
  return (
    <Layout className flex="row">
    <Header>Categories</Header>
     {categories.map(c => {
       return (
        <Card
           name={c.name}
           key={c.id}
           cta={{title: "View", onClick: ()=>{
             router.push(`/cat-search?category=${c.name}`)
           }}}
         />
      )
     })}
    </Layout>
  )
}

CategoriesPage.getInitialProps = async () => {
  const config = {
    headers: {
      'x-api-key': process.env.CAT_API_KEY,
    }
  };

  const URL='https://api.thecatapi.com/v1/categories';
  
  const CategoriesRequest = await request.get(URL, config);

  return {
    categories: CategoriesRequest.data,
  }
}

export default CategoriesPage;