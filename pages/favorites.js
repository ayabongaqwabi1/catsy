import styled from "styled-components";
import Layout from "../components/layout";
import Card from "../components/ImageCard";
import Header from "../components/PageHeader";

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  margin-top: 1em;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
`;

const FavoritesPage = () => {
  let cats = [];

  if(typeof window !== "undefined"){
    cats = JSON.parse(localStorage.getItem("catsy_favorites"))
  }

  return (
    <Layout
      flex="row"
      noFooter
    >
      <Header>Favorites</Header>
      <Container id="cat-search-container">        
            {cats.map((c) => {
              return <Card key={c} image={c} />;
            })}
      </Container>
    </Layout>
  );
};

export default FavoritesPage;
