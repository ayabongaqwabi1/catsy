import Image from 'next/image'
import styled from 'styled-components';
import config from '../config';
import NavLinks from '../components/NavLinks';
import Layout from '../components/layout';

const StyledHeader = styled.h1`
  font-family: 'Knewave',cursive;
  color: hotpink;
  font-size: 5em;
  text-align: center;
  padding: 20px;    
  text-shadow: 0 0 5px #00000038;
`;

const StyledPar = styled.p`
    color: #494848;
    font-family: 'Roboto', sans-serif;
    font-size: 2em;
    @media(max-width: 768px){
      font-size: 1.3em;
      padding: 10px;
      text-align: center;
    }
`;


export default function Home() {
  return (
      <Layout noHeader>
        <StyledHeader>{config.siteName}</StyledHeader>      
        <Image src="/cat-home-vector.png" alt="Vercel Logo" width={400} height={200} />
        <StyledPar>
          Find your favorite pictures of our furry little friends.
        </StyledPar>
        <NavLinks />
      </Layout>
  )
};
