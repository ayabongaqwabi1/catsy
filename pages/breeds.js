
import Image from 'next/image'
import styled from 'styled-components';
import Layout from '../components/layout';
import request from 'axios';
import Card from '../components/Card';
import { isEmpty, isNil } from 'ramda';
import { useRouter } from 'next/router'
import Header from "../components/PageHeader";


const BreedsPage = ({ breeds }) => {
  const router = useRouter();
  return (
    <Layout className flex="row">
      <Header>Categories</Header>
     {breeds.map(b => {
       return (
        <Card
           title={b.name}
           key={b.id}
           image={b.image ? b.image.url : ""}
           description={b.description}
           cta={{title: "View", onClick: ()=>{
            router.push(`/cat-search?breed=${b.id}`)
          }}}
           tags={b.temperament.split(",")}
           origin={b.origin}
         />
      )
     })}
    </Layout>
  )
}

BreedsPage.getInitialProps = async () => {
  console.log(process.env.CAT_API_KEY)
  const config = {
    headers: {
      'x-api-key': process.env.CAT_API_KEY,
    }
  };

  const URL='https://api.thecatapi.com/v1/breeds';
  
  const BreedsRequest = await request.get(URL, config);

  return {
    breeds: BreedsRequest.data,
  }
}

export default BreedsPage;