import styled from "styled-components";
import Layout from "../components/layout";
import request from "axios";
import Card from "../components/ImageCard";
import InfiniteScroll from "react-infinite-scroller";
import { isEmpty, isNil, union } from "ramda";
import { useState } from "react";
import {
  getBreeDatadFromQuery,
  getCategoryDataFromQuery
} from "../utils/data-processing";
import Header from "../components/PageHeader";
import Image from 'next/image'

import Link from 'next/link';

const exists = (i) => !isNil(i) && !isEmpty(i);

const config = {
  headers: {
    "x-api-key": process.env.CAT_API_KEY,
  },
};


const Container = styled.div`
  height: 100vh;
  width: 100vw;
  margin-top: 1em;
  .scroller{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
  }
`;

const Loader = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1em;
`;

const StyledH1 = styled.h1`
  text-align: center;
  font-size: 2em;
  padding: 10px;
  color: #b61565;
`;

const LinkContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  &:hover{
     a{
      border: 3px solid hotpink;
      background: #fff !important;
      color: hotpink !important;
     }
    }
  a{
    padding: 10px;
    background: hotpink;
    color: #fff;
    border-radius: 10px;
    &:hover{
      border: 3px solid hotpink;
      background: #fff !important;
      color: hotpink !important;
    }
  };
`;
const SearchPage = ({ cats, is404, breedId, categoryId, pageName }) => {
  const [stateCats, setStateCats] = useState(cats);
  const [nextPage, setNextPage] = useState(2);

  const fetchData = async () => {
    const URL = `https://api.thecatapi.com/v1/images/search?${exists(breedId) ? `breed_id=${breedId}` : ""}${exists(categoryId) ? `category_ids=${categoryId}` : ""}&page=${nextPage}&limit=10`;
    const BreedRequest = await request.get(URL, config);
    await setStateCats(
      union(
        BreedRequest.data.map((c) => c.url),
        stateCats
      )
    );
    await setNextPage(nextPage + 1);
  };

  return (
    <Layout
      flex="row"
      noFooter
    >
      <Header>{pageName}</Header>
      <Container id="cat-search-container">        
        {!is404 && exists(cats) && (
          <InfiniteScroll
            pageStart={0}
            loadMore={fetchData}
            hasMore={true}
            className="scroller"
            loader={
              <Loader>
               <Image src="/nyan.gif" alt="Catsy Logo" width={80} height={40} />
              </Loader>
            }
          >
            {stateCats.map((c) => {
              return <Card key={c} image={c} />;
            })}
          </InfiniteScroll>
        )}
        {is404 &&
         <>
          <StyledH1>Specified breed/category not found</StyledH1>
          <LinkContainer>
            <Link href="/">Go to homepage</Link>
          </LinkContainer>
         </>}
      </Container>
    </Layout>
  );
};

SearchPage.getInitialProps = async ({ query }) => {
  const { breedId, breedName } = await getBreeDatadFromQuery(query.breed);
  const { categoryId, categoryName } = await getCategoryDataFromQuery(query.category);

  const URL = `https://api.thecatapi.com/v1/images/search?${
    exists(breedId) ? `breed_id=${breedId}` : ""
  }${exists(categoryId) ? `category_ids=${categoryId}` : ""}&page=1&limit=10`;

  const RequestResult =
    exists(breedId) || exists(categoryId)
      ? await request.get(URL, config)
      : { data: [] };

  return {
    cats: RequestResult.data.map((c) => c.url),
    is404: !exists(breedId) && !exists(categoryId),
    breedId,
    categoryId,
    pageName: exists(breedId) ? breedName : categoryName,
  };
};

export default SearchPage;
