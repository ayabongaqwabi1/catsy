
import { append, isEmpty, isNil, without } from "ramda";
import styled from "styled-components";
import { FaRegHeart, FaHeart }from 'react-icons/fa';
import { useEffect, useState } from "react";

const Container = styled.div`
  width: 100%;
  height: auto;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media(min-width: 769px){
    width:200px;
  }
`;

const IconContainer = styled.div`
  position: absolute;
  margin-top: -100px;
  margin-left: 110px;
  background: white;
  border-radius: 50px;
  padding: 1em;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 1px 1px 8px 3px #00000021;
  @media(min-width: 769px){
    margin-left: 250px;
  }
  svg{
    fill: hotpink;
  }
`;

const ImageContainer = styled.div`
  background: url(${(props) => props.imageUrl}) no-repeat center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  height: 400px;
  z-index: 3;
`;

const exists = i => !isNil(i) && !isEmpty(i);


const Card = ({ name, cta }) => {

  return (
    <Container className="bg-pink-200 rounded-lg p-4 m-2">
      <h4 className="text-lg font-semibold capitalize">{name}</h4>
      <a
          className="p-2 leading-none rounded font-medium mt-3 bg-pink-400 text-xs"
          onClick={cta.onClick}
        >
          {cta.title}
      </a>
    </Container>
  );
};

export default Card;
