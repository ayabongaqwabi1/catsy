import styled from "styled-components";

const Header = styled.h1`
  text-align: center;
  width: 100%;
  text-shadow: 0 0 white;
  font-family: 'Knewave',cursive;
  color: hotpink;
  font-size: 5em;
  @media(max-width:768px){
        font-size: 2em;
    }
`;

export default Header;