import styled from "styled-components"
import config from '../../config';

const Route = styled.a`
    font-size: 1.2em;
    color: grey;
    &:hover{
        color: #5f5555;
    }
    &:after{
        content: "/";
        color: #5f5555;
        padding: 10px;
        font-weight: 600;
    }
`;

const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    z-index:3;
    ${Route}{
        &:last-of-type{
            &:after{
                content: "";
            }
        }
    }
`;

const NavLinks = () => {
    return (
        <Container>
            {config.routes.map( route => (
                <Route href={route.href} key={route.name}>{route.name}</Route>
            ))}
        </Container>
    )
}

export default NavLinks;