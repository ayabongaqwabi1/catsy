import Head from 'next/head'
import Image from 'next/image'
import styled from 'styled-components';
import config from '../../config';
import Header from '../Header';
import NavLinks from '../NavLinks';
import Particles from '../particles';


const StyledHeader = styled.h1`
  font-family: 'Knewave',cursive;
  color: hotpink;
  font-size: 5em;
  text-align: center;
  padding: 20px;    
  text-shadow: 0 0 5px #00000038;
`;

const MainContainer = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  
  
  #tsparticles{
    height: 100vh;
    position: fixed;
  }
`;

const StyledMain = styled.main`
  display: flex;
  flex-direction: ${props => props.flex ? props.flex: "column"};
  justify-content: center;
  align-items: center;
  ${props => props.flex ? `flex-flow: wrap;` : ""}
  padding-top: 5em;
`;

const StyledFooter = styled.footer`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;

  a {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-grow: 1;
    span{
      padding-left: 10px;
    }
  }
`;


export default function Layout({ children, flex, id, noFooter, noHeader }) {
  return (
    <MainContainer className id={id}>
      <Head>
        <title>{config.siteName}</title>
        <meta name="description" content="Find your favorite pictures of our furry little overlords." />
        <link rel="icon" href="/cat.ico" />        
      </Head>
      <Particles/>

      {!noHeader && 
        <Header />
      }

      <StyledMain flex={flex}>
        {children}
      </StyledMain>

      {!noFooter && 
        <StyledFooter>
        <a
          href="https://cars.co.za"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span>
            <Image src="/cars-logo.svg" alt="Cars Logo" width={72} height={26} />
          </span>
        </a>
      </StyledFooter>
      }
    </MainContainer>
  )
}
