
import { append, isEmpty, isNil, without } from "ramda";
import styled from "styled-components";
import { FaRegHeart, FaHeart }from 'react-icons/fa';
import { useEffect, useState } from "react";

const Container = styled.div`
  width: 100%;
  height: auto;
  z-index: 1;
  @media(min-width: 769px){
    width:600px;
  }
`;

const IconContainer = styled.div`
  position: relative;
  margin-top: 10px;
  background: #ffffff9e;
  border-radius: 50px;
  width: 70px;
  height: 70px;
  padding: 1em;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  box-shadow: rgba(0, 0, 0, 0.13) 1px 1px 8;
  
  svg{
    fill: hotpink;
  }
`;

const ImageContainer = styled.div`
  background: url(${(props) => props.imageUrl}) no-repeat center center;
  -webkit-background-size: contain;
  -moz-background-size: contain;
  -o-background-size: contain;
  background-size: contain;
  height: 250px;
  z-index: 3;
`;

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const exists = i => !isNil(i) && !isEmpty(i);


const Card = ({ image }) => {
  const [ isFavoritedImage, setIsFavoritedImage ] = useState(false);

  useEffect(()=>{
    setIsFavoritedImage(isFavorited(image))
  },[image]);

  const addToStorage = url=> {
    const currentFavorites = JSON.parse(localStorage.getItem("catsy_favorites"));
    const newFavorites = exists(currentFavorites) ? append(url, currentFavorites) : [url];
    localStorage.setItem("catsy_favorites", JSON.stringify(newFavorites));
    setIsFavoritedImage(true);
  };
  
  const removeFromStorage = url=> {
    const currentFavorites = JSON.parse(localStorage.getItem("catsy_favorites"));
    const newFavorites = exists(currentFavorites) ? JSON.stringify(without(url, currentFavorites)) : [];
    localStorage.setItem("catsy_favorites", newFavorites);
    setIsFavoritedImage(false)
  };
  
  const isFavorited = url => {
    if(typeof window === "undefined"){
      return false;
    }
    const currentFavorites = JSON.parse(localStorage.getItem("catsy_favorites"));
    return exists(currentFavorites) && currentFavorites.includes(url);
  }
  

  return (
    <Container className="bg-pink-200 rounded-lg p-4 m-2">
      {exists(image) &&
        <ImageContainer
        imageUrl={image}
      />
      }
      <Wrapper>
        <IconContainer onClick={isFavoritedImage ? () => removeFromStorage(image) : ()=> addToStorage(image)}>
          {!isFavoritedImage && <FaRegHeart size="2em" />}
          {isFavoritedImage && <FaHeart size="2em" />}
        </IconContainer>
      </Wrapper>
    </Container>
  );
};

export default Card;
