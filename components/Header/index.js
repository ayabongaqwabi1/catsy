import NavLinks from "../NavLinks";
import styled from "styled-components";
import Image from 'next/image';
import Link from 'next/link';


const Container = styled.div`
    position: fixed;
    top: 0;
    width: 100%;;
    z-index: 4;
    display: flex;
    background: #fff;
    padding: 1em;
    box-shadow: 1px 1px 5px 1px #00000026;
    justify-content: space-between;
    align-items: center;
    @media(max-width:768px){
        justify-content: flex-start;
    }
`;

const LinksContainer = styled.div`
    width: 30%;
    @media(max-width:769px){
        width: auto;
         margin-left: 10px;
        a{
            font-size: 1em;
        }
    }
`;

const Header = () => {
    return (
        <Container>
            <Link href="/">
                <Image src="/cat.png" alt="Catsy Logo" width={40} height={40} />
            </Link>
            <LinksContainer>
                <NavLinks />            
            </LinksContainer>
        </Container>
    )
}

export default Header;