import styled from "styled-components";
import { FaGlobeAfrica } from 'react-icons/fa';
import { isEmpty, isNil } from "ramda";

const exists = i => !isNil(i) && !isEmpty(i);

const ImageContainer = styled.div`
  background: url(${(props) => props.imageUrl}) no-repeat center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  height: 200px;
  z-index: 3;
`;


export const Container = styled.div`
  max-width: 300px;
  z-index: 1;
  a{
    cursor: pointer;
  }
`;

const TitleContainer = styled.div`
    display: flex;
    flex-direction: row;
    min-width: 200px;
    align-items: center;
    p{
        padding-left: 10px;
    }
    h4{
        padding-right: 10px;
    }
`;

const Card = ({ image, title, description, cta, tags, origin }) => {
  return (
    <Container className="flex flex-col bg-gray-200 rounded-lg p-4 m-2">
      {exists(image) &&
        <ImageContainer
        imageUrl={image}
      />
      }
      <div className="flex flex-col items-start mt-4">
        <TitleContainer>
            <h4 className="text-sm font-semibold">{title}</h4>
            <FaGlobeAfrica />
            <p>{origin}</p>
        </TitleContainer>
        <div className="flex flex-wrap mb-4 mt-2">
            {tags.map((t) => (
            <div
                key={t}
                className="text-xs inline-flex items-center font-bold leading-sm px-3 py-1 rounded-full bg-pink-200 text-gray-800 border"
            >
                {t}
            </div>
            ))}
        </div>
        
        <p className="text-sm">{description}</p>
        <a
          className="p-2 leading-none rounded font-medium mt-3 bg-pink-400 text-xs"
          onClick={cta.onClick}
        >
          {cta.title}
        </a>
      </div>
    </Container>
  );
};

export default Card;
